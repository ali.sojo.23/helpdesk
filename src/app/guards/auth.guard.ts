import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "../servicios/http/auth.service";
import { NgLocalization } from "@angular/common";

@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private route: Router) {}
  canActivate() {
    let logged = this.authService.verifyToken();
    if (!logged) {
      window.location.href = "/auth/login";
      return false;
    }
    return true;
  }
}
