import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { SlugPipe } from "../pipes/slug.pipe";

@Injectable({
  providedIn: "root"
})
export class UrlService {
  constructor(private slugPipe: SlugPipe) {}
  setPath() {
    let slug = this.slugPipe.transform(document.location.pathname);
    return slug;
  }
}
