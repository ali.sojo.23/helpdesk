import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  constructor(private httpClient: HttpClient) {}

  host = environment.host + "auth/";

  headers(token) {
    let header = new HttpHeaders().set("Content-Type", "application/json");
    return header;
  }
  login(data: any): Observable<any> {
    let Json = JSON.stringify(data);

    let headers = this.headers(null);

    return this.httpClient.get("http://localhost:8000/api/user");
  }
  register() {}
  logout() {}
  verifyToken() {
    let token: string = localStorage.getItem("tk_init");
    if (!token) {
      return false;
    } else {
      return true;
    }
  }
}
