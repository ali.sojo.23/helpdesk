import { Component } from "@angular/core";
import { SlugPipe } from "./pipes/slug.pipe";

@Component({
  selector: "[id=container]",
  host: { "[class]": "class" },
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  constructor(private slugPipe: SlugPipe) {
    this.setClass();
  }
  title = "hd";
  class = this.setClass();
  url: any = {};
  setClass() {
    this.url = this.slugPipe.transform(document.location.pathname);
    this.url = this.url.trim().split(" ");

    if (this.url[0] == "auth" || this.url[0] == null) {
      console.log(this.url);
      return "cls-container";
    } else {
      return "effect aside-float aside-bright mainnav-lg";
    }
  }
  classPriority() {
    if (this.url[1] == "register") {
      return "cls-content-lg";
    } else {
      return "cls-content-sm";
    }
  }
}
