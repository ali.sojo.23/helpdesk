import { Component, OnInit } from "@angular/core";
import { Title } from "@angular/platform-browser";

@Component({
  selector: "app-show",
  templateUrl: "./show.component.html",
  styleUrls: ["./show.component.css"]
})
export class ShowComponent implements OnInit {
  constructor(private title: Title) {
    this.title.setTitle("My Profile - HD");
  }

  ngOnInit() {}
}
