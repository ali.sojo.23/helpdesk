import { Component, OnInit } from "@angular/core";
import { Title } from "@angular/platform-browser";
import { HeadComponent } from "src/app/components/contentBox/container/head/head.component";

@Component({
  selector: "app-ccd",
  templateUrl: "./ccd.component.html",
  styleUrls: ["./ccd.component.css"]
})
export class CcdComponent implements OnInit {
  constructor(private titleService: Title, private HeadSlug: HeadComponent) {
    this.titleService.setTitle(this.title + " - HD");
    this.HeadSlug.setPath();
    this.HeadSlug.setTitle(this.title);
  }
  title = "Requests I'm CC'd on";
  ngOnInit() {}
}
