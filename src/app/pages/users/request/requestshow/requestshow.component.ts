import { Component, OnInit } from "@angular/core";
import { Title } from "@angular/platform-browser";
import { HeadComponent } from "src/app/components/contentBox/container/head/head.component";

@Component({
  selector: "app-requestshow",
  templateUrl: "./requestshow.component.html",
  styleUrls: ["./requestshow.component.css"]
})
export class RequestshowComponent implements OnInit {
  constructor(private titleService: Title, private HeadSlug: HeadComponent) {
    this.titleService.setTitle("Request - HD");
    this.HeadSlug.ngOnInit();
  }

  ngOnInit() {}
}
