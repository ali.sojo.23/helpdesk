import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/servicios/http/auth.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  constructor(private auth: AuthService) {}

  ngOnInit() {}
  email;
  password;

  login() {
    let data = {
      email: this.email,
      password: this.password
    };
    this.auth.login(data).subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.error(err);
      }
    );
  }
}
