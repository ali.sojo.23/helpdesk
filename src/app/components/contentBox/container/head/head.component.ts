import { Component, OnInit, OnChanges } from "@angular/core";
import { Router } from "@angular/router";
import { SlugPipe } from "src/app/pipes/slug.pipe";

@Component({
  selector: "[id='page-head']",
  templateUrl: "./head.component.html",
  styleUrls: ["./head.component.css"]
})
export class HeadComponent implements OnInit, OnChanges {
  constructor(public router: Router, private slugPipe: SlugPipe) {
    this.setPath();
  }
  slug: any;
  title: string;
  public ngOnInit() {
    this.setPath();
  }
  setPath() {
    this.slug = this.slugPipe.transform(document.location.pathname);
    this.slug = this.slug.trim().split(" ");
    console.log(this.slug);
  }
  setTitle(title) {
    this.title = title;
    console.log(title);
  }
  ngOnChanges() {
    console.log("checkig");
  }
}
