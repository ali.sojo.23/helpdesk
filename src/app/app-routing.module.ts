import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./pages/home/home.component";
import { NotfoundComponent } from "./pages/error/notfound/notfound.component";
import { RequestshowComponent } from "./pages/users/request/requestshow/requestshow.component";
import { CcdComponent } from "./pages/users/request/ccd/ccd.component";
import { LoginComponent } from "./pages/auth/login/login.component";
import { RegisterComponent } from "./pages/auth/register/register.component";
import { PasswordComponent } from "./pages/auth/password/password.component";
import { AuthGuard } from "./guards/auth.guard";
import { ShowComponent } from "./pages/users/profile/show/show.component";

const routes: Routes = [
  {
    path: "",
    component: HomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "request",
    component: RequestshowComponent
  },
  {
    path: "request/ccd",
    component: CcdComponent
  },
  {
    path: "auth",
    redirectTo: "auth/login"
  },
  {
    path: "auth/login",
    component: LoginComponent
  },
  {
    path: "auth/register",
    component: RegisterComponent
  },
  {
    path: "auth/forgot-password",
    component: PasswordComponent
  },
  {
    path: "users/profile",
    component: ShowComponent
  },
  {
    path: "**",
    redirectTo: "page-not-found"
  },
  {
    path: "page-not-found",
    component: NotfoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
