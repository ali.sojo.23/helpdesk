import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from "./app.component";
import { HeaderComponent } from "./components/navbar/header/header.component";
import { FooterComponent } from "./components/footer/footer/footer.component";
import { AsideComponent } from "./components/contentBox/aside/aside.component";
import { NavigationComponent } from "./components/contentBox/navigation/navigation.component";
import { HeadComponent } from "./components/contentBox/container/head/head.component";
import { HomeComponent } from "./pages/home/home.component";
import { NotfoundComponent } from "./pages/error/notfound/notfound.component";
import { RequestshowComponent } from "./pages/users/request/requestshow/requestshow.component";
import { CcdComponent } from "./pages/users/request/ccd/ccd.component";
import { SlugPipe } from "./pipes/slug.pipe";
import { UrlService } from "./servicios/url.service";
import { LoginComponent } from "./pages/auth/login/login.component";
import { RegisterComponent } from "./pages/auth/register/register.component";
import { PasswordComponent } from "./pages/auth/password/password.component";
import { ShowComponent } from "./pages/users/profile/show/show.component";

import { AuthService } from "./servicios/http/auth.service";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    AsideComponent,
    NavigationComponent,
    HeadComponent,
    HomeComponent,
    NotfoundComponent,
    RequestshowComponent,
    CcdComponent,
    SlugPipe,
    LoginComponent,
    RegisterComponent,
    PasswordComponent,
    ShowComponent
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule],
  providers: [SlugPipe, UrlService, HeadComponent, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule {}
