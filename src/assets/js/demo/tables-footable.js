// Tables-FooTable.js
// ====================================================================
// This file should not be included in your project.
// This is just a sample how to initialize plugins or components.
//
// - ThemeOn.net -

$(document).on("nifty.ready", function() {
  // FOO TABLES
  // =================================================================
  // Require FooTable
  // -----------------------------------------------------------------
  // http://fooplugins.com/footable-demos/
  // =================================================================

  // Row Toggler
  // -----------------------------------------------------------------
  $("#helpdesk-foo-row-toggler").footable();

  // Expand / Collapse All Rows
  // -----------------------------------------------------------------
  var fooColExp = $("#helpdesk-foo-col-exp");
  fooColExp.footable().trigger("footable_expand_first_row");

  $("#helpdesk-foo-collapse").on("click", function() {
    fooColExp.trigger("footable_collapse_all");
  });
  $("#helpdesk-foo-expand").on("click", function() {
    fooColExp.trigger("footable_expand_all");
  });

  // Accordion
  // -----------------------------------------------------------------
  $("#helpdesk-foo-accordion")
    .footable()
    .on("footable_row_expanded", function(e) {
      $("#helpdesk-foo-accordion tbody tr.footable-detail-show")
        .not(e.row)
        .each(function() {
          $("#helpdesk-foo-accordion")
            .data("footable")
            .toggleDetail(this);
        });
    });

  // Pagination
  // -----------------------------------------------------------------
  $("#helpdesk-foo-pagination").footable();
  $("#helpdesk-show-entries").change(function(e) {
    e.preventDefault();
    var pageSize = $(this).val();
    $("#helpdesk-foo-pagination").data("page-size", pageSize);
    $("#helpdesk-foo-pagination").trigger("footable_initialized");
  });

  // Filtering
  // -----------------------------------------------------------------
  var filtering = $("#helpdesk-foo-filtering");
  filtering.footable().on("footable_filtering", function(e) {
    var selected = $("#helpdesk-foo-filter-status")
      .find(":selected")
      .val();
    e.filter += e.filter && e.filter.length > 0 ? " " + selected : selected;
    e.clear = !e.filter;
  });

  // Filter status
  $("#helpdesk-foo-filter-status").change(function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", { filter: $(this).val() });
  });

  // Search input
  $("#helpdesk-foo-search").on("input", function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", { filter: $(this).val() });
  });

  // Add & Remove Row
  // -----------------------------------------------------------------
  var addrow = $("#helpdesk-foo-addrow");
  addrow.footable().on("click", ".helpdesk-delete-row", function() {
    //get the footable object
    var footable = addrow.data("footable");

    //get the row we are wanting to delete
    var row = $(this).parents("tr:first");

    //delete the row
    footable.removeRow(row);
  });

  // Search input
  $("#helpdesk-input-search2").on("input", function(e) {
    e.preventDefault();
    addrow.trigger("footable_filter", { filter: $(this).val() });
  });

  // Add Row Button
  $("#helpdesk-btn-addrow").click(function() {
    //get the footable object
    var footable = addrow.data("footable");

    //build up the row we are wanting to add
    var newRow =
      '<tr><td><button class="helpdesk-delete-row btn btn-danger btn-xs"><i class="helpdesk-pli-cross"></i></button></td><td>Adam</td><td>Doe</td><td>Traffic Court Referee</td><td>22 Jun 1972</td><td><span class="label label-table label-success">Active</span></td></tr>';

    //add it
    footable.appendRow(newRow);
  });
});
